package com.covid19.drawdatacovid19.kafka

import com.covid19.drawdatacovid19.model.CovidCountry
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate

class Producer {
    @Autowired
    private lateinit var kafkaTemplate: KafkaTemplate<String, CovidCountry>

    fun sendMessage(topic: String, message: CovidCountry ){
        kafkaTemplate.send(topic, message)
    }
}