package com.covid19.drawdatacovid19.kafka

import org.apache.kafka.clients.admin.NewTopic
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.config.TopicBuilder

@Configuration
class KafkaTopicConfig {
    @Bean
    fun covidCountryTopic(): NewTopic{
        return TopicBuilder.name("covidcountry")
            .replicas(2).partitions(3).build()
    }
}