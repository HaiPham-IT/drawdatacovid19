package com.covid19.drawdatacovid19.service.impl

import com.covid19.drawdatacovid19.dao.CovidCountryDao
import com.covid19.drawdatacovid19.kafka.Producer
import com.covid19.drawdatacovid19.model.CovidCountry
import com.covid19.drawdatacovid19.resources.CoronaTracker
import com.covid19.drawdatacovid19.resources.Diasease
import com.covid19.drawdatacovid19.service.CovidCountryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CovidCountryServiceImpl: CovidCountryService {
    @Autowired
    private lateinit var covidCountryDao: CovidCountryDao
    override fun crawData(resource: String) {
        val data: List<CovidCountry>? = when(resource){
            "Disease" -> Diasease().getTotalForAllCountries()
            "Coronatracker" -> CoronaTracker().getTotalForAllCountries()
            else -> null
        }
        data?.forEach { d: CovidCountry -> handleRecord(d) }
    }
    private fun handleRecord(covidCountry: CovidCountry){
        when(val oldRd = covidCountryDao.findOneByCountryAndSource(covidCountry.country, covidCountry.source)) {
            null -> {
                val newRd = covidCountryDao.insert(covidCountry)
                Producer().sendMessage("covidcountry", newRd)
            }
            else -> {
                if(isChange(oldRd, covidCountry)) {
                    covidCountry.also { it.id  = oldRd.id}
                    val newRd = covidCountryDao.save(covidCountry)
                    Producer().sendMessage("covidcountry", newRd)
                }
            }
        }
    }
    private fun isChange(oldRd: CovidCountry, newRd: CovidCountry): Boolean {
        return !oldRd.continent.equals(newRd.continent, true) || oldRd.cases != newRd.cases
                || oldRd.deaths != newRd.deaths || oldRd.recovered != newRd.recovered
    }
}