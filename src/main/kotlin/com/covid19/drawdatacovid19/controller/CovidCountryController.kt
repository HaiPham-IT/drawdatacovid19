package com.covid19.drawdatacovid19.controller

import com.covid19.drawdatacovid19.service.impl.CovidCountryServiceImpl
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
@RequestMapping("/v1/api")
class CovidCountryController(private var covidCountryService: CovidCountryServiceImpl) {

    @GetMapping("/drawdata")
    fun drawData(@RequestParam resource: String): ResponseEntity<String>{
        covidCountryService.crawData(resource)
        return ResponseEntity.ok("Draw data successfully")
    }
}