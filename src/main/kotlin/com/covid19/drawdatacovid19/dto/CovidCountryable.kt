package com.covid19.drawdatacovid19.dto

import com.covid19.drawdatacovid19.model.CovidCountry

interface CovidCountryable {
    fun toCovidCountry(): CovidCountry
}