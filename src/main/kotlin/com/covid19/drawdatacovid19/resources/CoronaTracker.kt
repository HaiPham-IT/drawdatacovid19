package com.covid19.drawdatacovid19.resources

import com.covid19.drawdatacovid19.utils.createHttpClient
import com.covid19.drawdatacovid19.utils.createRequest
import com.covid19.drawdatacovid19.utils.sendRequest
import com.covid19.drawdatacovid19.dto.TrackerCovidCountry
import com.covid19.drawdatacovid19.model.CovidCountry
import com.google.gson.Gson

class CoronaTracker{
    private val endpoint: String = "https://api.coronatracker.com/"
    private val total4allcountries: String = "v3/stats/worldometer/country"
    fun getTotalForAllCountries(): List<CovidCountry> {
        val client = createHttpClient()
        val request = createRequest(endpoint, total4allcountries, null)
        val response = sendRequest(client, request)
        val trackerCovidCountries = Gson().fromJson(response.body(), Array<TrackerCovidCountry>::class.java).toList()
        return trackerCovidCountries.map { it.toCovidCountry() }
    }
}

fun main() {
    CoronaTracker().getTotalForAllCountries()
}