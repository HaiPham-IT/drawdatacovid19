package com.covid19.drawdatacovid19.resources

import com.covid19.drawdatacovid19.utils.*
import com.covid19.drawdatacovid19.dto.DiseaseCovidCountry
import com.covid19.drawdatacovid19.model.CovidCountry
import com.google.gson.Gson

class Diasease{
    private val endpoint: String = "https://disease.sh/"
    private val total4allcountries: String = "v3/covid-19/countries/"
    fun getTotalForAllCountries(): List<CovidCountry> {
        val client = createHttpClient()
        val request = createRequest(endpoint, total4allcountries, null)
        val response = sendRequest(client, request)
        val diseaseTotalAllCountries = Gson().fromJson(response.body(), Array<DiseaseCovidCountry>::class.java).toList()
        return diseaseTotalAllCountries.map { it.toCovidCountry() }
    }
}

fun main() {
    Diasease().getTotalForAllCountries()
}