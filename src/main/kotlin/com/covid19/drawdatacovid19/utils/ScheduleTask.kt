package com.covid19.drawdatacovid19.utils

import com.covid19.drawdatacovid19.service.impl.CovidCountryServiceImpl
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class ScheduleTask(private var covidCountryService: CovidCountryServiceImpl) {
    @Scheduled(fixedDelay = 1000 * 60 * 60 * 3)
    fun getCovid19Data(){
        covidCountryService.crawData("Disease")
        covidCountryService.crawData("Coronatracker")
    }
}