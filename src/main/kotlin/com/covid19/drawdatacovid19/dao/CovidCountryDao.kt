package com.covid19.drawdatacovid19.dao

import com.covid19.drawdatacovid19.model.CovidCountry
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query

interface CovidCountryDao : MongoRepository<CovidCountry, String> {
    @Query("{country: '?0', source: '?1'}")
    fun findOneByCountryAndSource(country: String?, source: String?): CovidCountry?
}